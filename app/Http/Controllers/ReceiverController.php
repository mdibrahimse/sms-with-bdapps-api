<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SMS;
use App\Classes\SMSReceiver;
use App\Classes\SMSSender;
use App\Classes\Core;
use App\Classes\DirectDebitSender;
use App\Classes\CassException;
use App\Classes\UssdReceiver;
use App\Classes\UssdSender;
use App\Classes\UssdException;
use App\Subscription;
use App\Charging;
use App\USSD;
use App\USSD_SESSION;


class ReceiverController extends SMSReceiver
{

    public function testSMS(Request $request){
       $err='';
       $row=new Subscription;
	   $data=new SMS(); 
	   $value=new Charging();
	   $sms_send=new SMSSender('http://localhost:7000/sms/send',$this->getAddress(),'123');
	//divided more string using explode
	   $explode= explode(' ', $this->getMessage());
	   $message= end($explode);

	   $address=new SMSReceiver();
	   $user = Subscription::where('msisdn',$address->getAddress('msisdn'))->first();
	   
    //condition for checking balence with message unsub
    if ($err=='E1308' && $message=='unsub' && $user['status']==1) {	   			  			
	   	  		$row= Subscription::where('msisdn',$address->getAddress('msisdn'))->first();
	   	  		$row->status=0;	  
	   			$sms_send->sms('you are successfully unsubscribe',$this->getAddress());	
	   			$row->save();
	   			
	   			}elseif ($err=='E1308' && $message=='unsub' && $user['status']==0) {
	   				$sms_send->sms('you are not subscribe user',$this->getAddress());
	   			
	}else{ //condition for checking balence with message sub

	   		$address->getAddress();
	   
	   		if(count($user)>0){ //condition for checking existing user
	   				$charge=new DirectDebitSender('http://localhost:7000/caas/direct/debit',$this->getApplicationId(),'123');
					$check=$charge->cass('321',$this->getAddress(),'10');
	   		if ($message=='sub'&& $user['status']==0 && $check=='ok') {
                //for amount charging
                $charge=new DirectDebitSender('http://localhost:7000/caas/direct/debit',$this->getApplicationId(),'123');
                $charge->cass('321',$this->getAddress(),'10');      
                $err=$charge->getstatusCode();

	   			$row= Subscription::where('msisdn',$address->getAddress('msisdn'))->first();
	   	  		$row->status=1;	  
	   			$sms_send->sms('you are successfully subscribe',$this->getAddress());	
	   			$row->save();
	   		 }elseif ($message=='sub' && $user['status']==0 && $check!='ok') {	
        		$sms_send->sms('Insufficient Balance',$this->getAddress());	
 	   			
 	   		}elseif ($message=='sub' && $user['status']==1) {
	   				$sms_send->sms('you are already subscribed',$this->getAddress());	   			
	   		}elseif ($message=='unsub' && $user['status']==1) {	   			  			
	   	  		$row= Subscription::where('msisdn',$address->getAddress('msisdn'))->first();
	   	  		$row->status=0;	  
	   			$sms_send->sms('you are successfully unsubscribe',$this->getAddress());	
	   			$row->save();
	   		
	   		}elseif ($message=='unsub' && $user['status']==0) {
	   				$sms_send->sms('you are already unsubscribed',$this->getAddress());
	   			
	   		}elseif($message!='sub' && $message!='unsub'){
	   		 	$sms_send->sms('Please Send Valid Message',$this->getAddress());
	   		 }
	   	} //end of checking existing user

	else{ //condition for new user
		$charge=new DirectDebitSender('http://localhost:7000/caas/direct/debit',$this->getApplicationId(),'123');
		$check=$charge->cass('321',$this->getAddress(),'10'); 
	  if ($message=='sub' && $check=='ok') {	                                   
                $err=$charge->getstatusCode();
	   	  		$row->msisdn=$this->getAddress();
	   	 		$row->status=1;	   	 
	   			$sms_send->sms('you are successfully subscribe',$this->getAddress());	
	   			$row->save();	   		
	   	
 	  	}elseif ($message=='sub' && $check!='ok') {	      	   	  	   	 
 	   			$sms_send->sms('Insufficient Balance',$this->getAddress());		   			
 	   	}elseif($message=='unsub'){		   			 
	   			$sms_send->sms('you are not Subscribe user',$this->getAddress());	   		
	   	}elseif($message!='sub' && $message!='unsub'){
	   		 	$sms_send->sms('Please Send Valid Message',$this->getAddress());
	   		 	}

	   	}//end of new user block
	   } //end of checking balence with message sub

	   //store data of charging
	   
	   $req=$charge->getRaw_request();
	   $res=$charge->getRaw_response();
	   $value->ch_raw_request=$req;
	   $value->ch_raw_response=json_encode($res);
	   $code=$charge->getstatusCode();
	   $value->status_code=$code;
	   $detail=$charge->getstatusDetail();
	   $value->status_detail=$detail;
	   $value->save();


	   // store total request
	   	$data->raw_receive=json_encode($this->getJson());
	   	$raw_request=$sms_send->getRawrequest();	 
		$data->raw_request=json_encode($raw_request);
		$raw_response=$sms_send->getRawresponse();
		$data->raw_response=json_encode($raw_response);

		$ab=$sms_send->status_code();
		$abc=$sms_send->status_detail();
	    $data->status_code=$ab;
		$data->status_details=$abc;

	    $data->save(); 
	} 


// USSD part .............................USSD part................
	public function getUSSD(Request $request){
		$value=new USSD; 
		$data=new USSD_SESSION;
		$chargingAmount='';
		$msg='';
	    $ussdR_obj=new UssdReceiver();
	   $ussd_result=$ussdR_obj->getthejson();
	   $appId=$ussdR_obj->getApplicationId();
	   $sesId=$ussdR_obj->getSessionId();
	   $desAdd=$ussdR_obj->getAddress();
	   $UsOp=$ussdR_obj->getUssdOperation();
	   $getmsg=$ussdR_obj->getMessage();
	   $ussdS_obj=new UssdSender('http://localhost:7000/ussd/send',$appId,'123');
	   $charge=new DirectDebitSender('http://localhost:7000/caas/direct/debit',$this->getApplicationId(),'123');
	
	    //
	    $chargingAmount=$charge->cass('321',$desAdd,'0');
	    if($UsOp=='mo-init'){
	    	$msg = '1 = Poem <br>2 = Song <br> 0 = exit';
	    	$ussdSend_data=$ussdS_obj->ussd($sesId,$msg,$desAdd,$UsOp);
	    	$user = USSD_SESSION::where('msisdn',$ussdR_obj->getAddress('msisdn'))->first();
				if(count($user)==0){
				$data->msisdn=$desAdd;
				$data->save();
				}
		}
	
				
	    if ($chargingAmount=='ok' && $getmsg=='1' && $UsOp=='mo-cont') {
	    	$chargingAmount=$charge->cass('321',$desAdd,'2');
	    	$ussdSend_data=$ussdS_obj->ussd($sesId,'Poem is active now<br># Main Menu',$desAdd,$UsOp);
	    	}elseif($chargingAmount=='no' && $getmsg=='1' && $UsOp=='mo-cont'){
	    		$ussdSend_data=$ussdS_obj->ussd($sesId,'Insufficient balence # Main Menu',$desAdd,$UsOp);
	    	}
	    
	    	elseif ($chargingAmount=='ok' && $getmsg=='2' && $UsOp=='mo-cont') { 
	    		$chargingAmount=$charge->cass('321',$desAdd,'2');
	    	$ussdSend_data=$ussdS_obj->ussd($sesId,'Song is active now<br># Main Menu',$desAdd,$UsOp);
		    }elseif($chargingAmount=='no' && $getmsg=='2' && $UsOp=='mo-cont'){
	    		$ussdSend_data=$ussdS_obj->ussd($sesId,'Insufficient balence # Main Menu',$desAdd,$UsOp);
	    	}elseif ($getmsg=='#' && $UsOp=='mo-cont') {
	    		
	    		$ussdSend_data=$ussdS_obj->ussd($sesId,'1 = Poem <br>2 = Song <br> 0 = exit',$desAdd,$UsOp);
	    	
	    }

	    if ($getmsg=='0' && $UsOp=='mo-cont') {
	    	$user = USSD_SESSION::where('msisdn',$desAdd)->first();
				if(count($user)==1){						
				$user->delete();
				$ussdSend_data=$ussdS_obj->ussd($sesId,'Thanks, successfully exit',$desAdd,$UsOp);
				}else{
					$ussdSend_data=$ussdS_obj->ussd($sesId,'Please dial with valid pin',$desAdd,$UsOp);
				}
			}
	    
	
	    //store data
	    $value->raw_receive=json_encode($ussd_result);
	    $req=$ussdS_obj->getRaw_request();
	    $value->raw_request=$req;	    
	    $res=$ussdS_obj->getRaw_response();
	    $value->raw_reponse=$res;	    
	    $code=$ussdS_obj->getStatus_code();
	    $detail=$ussdS_obj->getStatus_details();
	    $value->status_code=$code;	   
	    $value->status_details=$detail;
	    $value->save();
	   
	}
} 





			