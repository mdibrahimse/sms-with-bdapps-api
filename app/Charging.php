<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charging extends Model
{
    protected $table='charging';
    protected $fillable=['ch_raw_request','ch_raw_response','status_code','status_detail'];
}
