<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class USSD extends Model
{
   protected $table='ussd';
   protected $fillable=['raw_receive','raw_request','raw_reponse','status_code','status_details'];
}
