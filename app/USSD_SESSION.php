<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class USSD_SESSION extends Model
{
    protected $table='ussd_session';
    protected $fillable=['msisdn'];
}
