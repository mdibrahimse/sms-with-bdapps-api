<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMS extends Model
{
    protected $table='allsms';
    protected $fillable=['raw_receive','raw_request','raw_response','status_code','status_details'];
}
