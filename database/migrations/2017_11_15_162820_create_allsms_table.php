<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllsmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allsms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('raw_recive');
            $table->string('raw_request');
            $table->string('raw_response');
            $table->string('status_code');
            $table->string('status_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allsms');
    }
}
